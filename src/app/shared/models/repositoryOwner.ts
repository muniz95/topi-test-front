export class RepositoryOwner {
    login: String;
    id: Number;
    avatarUrl: String;
    gravatarId: String;
    url: String;
    htmlUrl: String;
    followersUrl: String;
    followingUrl: String;
    gistsUrl: String;
    starredUrl: String;
    subscriptionsUrl: String;
    organizationsUrl: String;
    reposUrl: String;
    eventsUrl: String;
    receivedEventsUrl: String;
    type: String;
    siteAdmin: Boolean;
}

export class License {
    key: String;
    name: String;
    spdxId: String;
    url: String;
}

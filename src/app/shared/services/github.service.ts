import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Repository } from '../models/repository';
import 'rxjs/add/operator/map';

@Injectable()
export class GithubService {

    private url = 'https://topi-test-rodrigo-muniz.herokuapp.com/repos';
    private headers: Headers;
    private options: RequestOptions;

    constructor(private http: Http) {
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        });
        this.options = new RequestOptions({ headers: this.headers });
    }

    getRepositories(id) {
        return this.http.get(this.getRepositoriesUrl(id), this.options)
            .map(res => res.json() as Repository[]);
    }

    private getRepositoriesUrl(id) {
        return this.url + '/?language=' + id;
    }

}

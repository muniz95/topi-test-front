import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';

import { GithubService } from '../shared/services/github.service';
import { RepoCardComponent } from './repo-card/repo-card.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HomeComponent, RepoCardComponent],
  providers: [
    GithubService
  ]
})
export class HomeModule { }
